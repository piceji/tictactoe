﻿using GameCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool _currentPlayer;
        private readonly IBoard _board;
        private CellState? _winner = CellState.None;
        private readonly Label _statusLine;

        public MainWindow()
        {
            InitializeComponent();
            _board = new Board();

            _statusLine = new Label
            {
                Content = "Player one is playing",
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Bottom,
                Width = 232,
                Height = 30
            };
            TicTacToeGrid.Children.Add(_statusLine);
        }

        /// <summary>
        /// Handles button click. It doesn't use background threads because it's a draw based game without other controlls.
        /// </summary>
        /// <param name="sender">Clicked button</param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (_winner != CellState.None)
                return;

            try
            {
                if (!MarkCell((Button) sender)) return;
                
                CheckEnd();
                SwitchPlayer();
            }
            catch (Exception ex)
            {
#if DEBUG
                _statusLine.Content = ex.Message;
#else
                _statusLine.Content = "An error occurred, restart game.";
#endif
            }
        }

        /// <summary>
        /// Sends request to GameCore to save status and sets mark for clicked button according to current player.
        /// </summary>
        /// <param name="button">Clicked button</param>
        /// <returns>When the cell is occupied by any player it returns false, true otherwise</returns>
        private bool MarkCell(ContentControl button)
        {
            var tag = button.Tag;
            var coordinates = tag.ToString().Split(',').Select(int.Parse).ToArray();

            if (_board.GetCellStatus(coordinates) != CellState.None) return false;
            _statusLine.Content = "Processing";

            _board.SetCellStatus(coordinates, _currentPlayer);
            button.Content = _currentPlayer ? "O" : "X";
            return true;
        }

        /// <summary>
        /// Makes the other players draw.
        /// </summary>
        private void SwitchPlayer()
        {
            if (_winner != CellState.None) return;
            _currentPlayer = !_currentPlayer;
            _statusLine.Content = $"Player {(_currentPlayer ? "two" : "one")} is playing";
        }

        /// <summary>
        /// Checks if any player already won.
        /// </summary>
        private void CheckEnd()
        {
            _winner = _board.CheckWinner();

            if (_winner == CellState.None) return;

            if (!_winner.HasValue)
            {
                _statusLine.Content = "Draw";
                return;
            }

            _statusLine.Content = $"Player {(_currentPlayer ? "two" : "one")} is winner";
        }
    }
}