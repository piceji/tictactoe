using System.Collections.Generic;
using GameCore.Exceptions;
using NUnit.Framework;

namespace GameCore.Tests
{
    public class Tests
    {
        Board testBoard;

        [SetUp]
        public void Setup()
        {
            testBoard = new Board();
        }

        [Test]
        [TestCase(0,0)]
        [TestCase(0,1)]
        [TestCase(0,2)]
        [TestCase(1,0)]
        [TestCase(1,1)]
        [TestCase(1,2)]
        [TestCase(2,0)]
        [TestCase(2,1)]
        [TestCase(2,2)]
        public void TestGetCellStatus_EmptyBoard(int x, int y)
        {
            Assert.AreEqual(CellState.None, testBoard.GetCellStatus(x, y));
        }
        
        [Test]
        [TestCase(0,0)]
        [TestCase(0,1)]
        [TestCase(0,2)]
        [TestCase(1,0)]
        [TestCase(1,1)]
        [TestCase(1,2)]
        [TestCase(2,0)]
        [TestCase(2,1)]
        [TestCase(2,2)]
        public void TestGetCellStatusArray_EmptyBoard(int x, int y)
        {
            Assert.AreEqual(CellState.None, testBoard.GetCellStatus(new[] {x, y}));
        }
        
        /*[Test]
        [TestCase(0,0, CellState.None)]
        [TestCase(0,1, CellState.None)]
        [TestCase(0,2, CellState.None)]
        [TestCase(1,0, CellState.None)]
        [TestCase(1,1, CellState.None)]
        [TestCase(1,2, CellState.None)]
        [TestCase(2,0, CellState.None)]
        [TestCase(2,1, CellState.None)]
        [TestCase(2,2, CellState.None)]
        [TestCase(0,0, CellState.PlayerOne)]
        [TestCase(0,1, CellState.PlayerOne)]
        [TestCase(0,2, CellState.PlayerOne)]
        [TestCase(1,0, CellState.PlayerOne)]
        [TestCase(1,1, CellState.PlayerOne)]
        [TestCase(1,2, CellState.PlayerOne)]
        [TestCase(2,0, CellState.PlayerOne)]
        [TestCase(2,1, CellState.PlayerOne)]
        [TestCase(2,2, CellState.PlayerOne)]
        [TestCase(0,0, CellState.PlayerTwo)]
        [TestCase(0,1, CellState.PlayerTwo)]
        [TestCase(0,2, CellState.PlayerTwo)]
        [TestCase(1,0, CellState.PlayerTwo)]
        [TestCase(1,1, CellState.PlayerTwo)]
        [TestCase(1,2, CellState.PlayerTwo)]
        [TestCase(2,0, CellState.PlayerTwo)]
        [TestCase(2,1, CellState.PlayerTwo)]
        [TestCase(2,2, CellState.PlayerTwo)]
        public void TestSetCellStatus(int x, int y, CellState status)
        {
            Assert.AreEqual(CellState.None, testBoard.GetCellStatus(new[] {x, y}));
            testBoard.SetCellStatus(x, y, status);
            Assert.AreEqual(status, testBoard.GetCellStatus(x, y));
        }*/
        
        [Test]
        [TestCase(0,0, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(0,1, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(0,2, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(1,0, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(1,1, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(1,2, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(2,0, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(2,1, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(2,2, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(0,0, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(0,1, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(0,2, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(1,0, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(1,1, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(1,2, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(2,0, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(2,1, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(2,2, true, ExpectedResult = CellState.PlayerTwo)]
        public CellState TestSetCellStatusArray(int x, int y, bool player)
        {
            Assert.AreEqual(CellState.None, testBoard.GetCellStatus(new[] {x, y}));
            testBoard.SetCellStatus(new[]{x, y}, player);
            return testBoard.GetCellStatus(x, y);
        }
        
               
        [Test]
        [TestCase(0, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(1, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(2, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(0, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(1, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(2, true, ExpectedResult = CellState.PlayerTwo)]
        public CellState? TestCheckWinner_RowWin(int row, bool player)
        {
            testBoard.SetCellStatus(new []{row, 0}, player);
            testBoard.SetCellStatus(new []{row, 1}, player);
            testBoard.SetCellStatus(new []{row, 2}, player);
            return testBoard.CheckWinner();
        }
        
        [Test]
        [TestCase(0, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(1, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(2, false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(0, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(1, true, ExpectedResult = CellState.PlayerTwo)]
        [TestCase(2, true, ExpectedResult = CellState.PlayerTwo)]
        public CellState? TestCheckWinner_ColWin(int column, bool player)
        {
            testBoard.SetCellStatus(new []{0, column}, player);
            testBoard.SetCellStatus(new []{1, column}, player);
            testBoard.SetCellStatus(new []{2, column}, player);
            return testBoard.CheckWinner();
        }
        
        [Test]
        [TestCase(false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(true, ExpectedResult = CellState.PlayerTwo)]
        public CellState? TestCheckWinner_Diagonal1(bool player)
        {
            testBoard.SetCellStatus(new []{0, 0}, player);
            testBoard.SetCellStatus(new []{1, 1}, player);
            testBoard.SetCellStatus(new []{2, 2}, player);
            return testBoard.CheckWinner();
        }
        
        [Test]
        [TestCase(false, ExpectedResult = CellState.PlayerOne)]
        [TestCase(true, ExpectedResult = CellState.PlayerTwo)]
        public CellState? TestCheckWinner_Diagonal2(bool player)
        {
            testBoard.SetCellStatus(new []{2, 0}, player);
            testBoard.SetCellStatus(new []{1, 1}, player);
            testBoard.SetCellStatus(new []{0, 2}, player);
            return testBoard.CheckWinner();
        }
        
        [Test]
        public void TestCheckWinner_EmptyBoard()
        {
            Assert.AreEqual(CellState.None, testBoard.CheckWinner());
        }
        
        [Test]
        public void TestCheckWinner_Draw()
        {
            /*
             * Board pattern:
             * xox
             * xox
             * oxo
             */
            testBoard.SetCellStatus(new []{0,0}, false);
            testBoard.SetCellStatus(new []{0,1}, true);
            testBoard.SetCellStatus(new []{0,2}, false);
            testBoard.SetCellStatus(new []{1,0}, false);
            testBoard.SetCellStatus(new []{1,1}, true);
            testBoard.SetCellStatus(new []{1,2}, false);
            testBoard.SetCellStatus(new []{2,0}, true);
            testBoard.SetCellStatus(new []{2,1}, false);
            testBoard.SetCellStatus(new []{2,2}, true);
            Assert.IsNull(testBoard.CheckWinner());
        }

        [Test]
        [TestCase(0, 3)]
        [TestCase(3, 0)]
        [TestCase(3, 3)]
        [TestCase(-1, 0)]
        [TestCase(0, -1)]
        [TestCase(-1, -1)]
        [TestCase(-1, 3)]
        [TestCase(3, -1)]
        public void TestSetCellStatus_ExceptionOutOfBoard(int x, int y)
        {
            Assert.Throws<IndexOutOfBoardException>(() => testBoard.SetCellStatus(new[]{x, y}, false));
        }
        
        [Test]
        public void TestSetCellStatus_ExceptionInvalidArgument_TooMuch()
        {
            Assert.Throws<InvalidArgumentException>(() => testBoard.SetCellStatus(new[]{0,0,0}, false));
        }
        
        [Test]
        public void TestSetCellStatus_ExceptionInvalidArgument_TooFew()
        {
            Assert.Throws<InvalidArgumentException>(() => testBoard.SetCellStatus(new[]{0}, false));
        }
    }
}