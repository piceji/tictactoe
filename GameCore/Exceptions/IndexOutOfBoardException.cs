using System;

namespace GameCore.Exceptions
{
    /// <summary>
    /// Use when indexing cells out of border.
    /// </summary>
    [Serializable]
    public class IndexOutOfBoardException : Exception
    {
        public IndexOutOfBoardException(string message) : base(message)
        {
        }
    }
}