using System;

namespace GameCore.Exceptions
{
    /// <summary>
    /// Use when content of argument doesn't match expected data.
    /// </summary>
    [Serializable]
    public class InvalidArgumentException : Exception
    {
        public InvalidArgumentException(string message) : base($"Invalid content of argument {message}")
        {
        }
    }
}