﻿namespace GameCore
{
    public interface IBoard
    {
        CellState? CheckWinner();
        CellState GetCellStatus(int x, int y);
        CellState GetCellStatus(int[] coordinates);
        void SetCellStatus(int[] coordinates, bool player);
    }
}