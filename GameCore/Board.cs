﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameCore.Exceptions;

namespace GameCore
{
    public class Board : IBoard
    {
        private const int XSize = 3;
        private const int YSize = 3;

        private readonly List<List<CellState>> _boardState = new List<List<CellState>>
        {
            new List<CellState> {CellState.None, CellState.None, CellState.None},
            new List<CellState> {CellState.None, CellState.None, CellState.None},
            new List<CellState> {CellState.None, CellState.None, CellState.None}
        };

        /// <summary>
        /// Sets cell identified by coordinates <paramref name="x"/> and <paramref name="y"/> to given <paramref name="status"/>
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="player">New status for cell</param>
        private void SetCellStatus(int x, int y, CellState player)
        {
            CheckRange(x, y);
            _boardState[y][x] = player;
        }

        /// <summary>
        /// Sets cell identified by <paramref name="coordinates"/> for given player
        /// </summary>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="player">New status for cell</param>
        public void SetCellStatus(int[] coordinates, bool player)
        {
            CheckCoordinatesCount(coordinates);
            SetCellStatus(coordinates[0], coordinates[1], player ? CellState.PlayerTwo : CellState.PlayerOne);
        }

        /// <summary>
        /// Returns status of cell specified by <paramref name="x"> and <paramref name="y"/> coordinates.
        /// </summary>
        /// <param name="x">Column number</param>
        /// <param name="y">Row number</param>
        /// <returns></returns>
        public CellState GetCellStatus(int x, int y)
        {
            CheckRange(x, y);
            return _boardState[y][x];
        }

        /// <summary>
        /// Returns status of cell specified by <paramref name="coordinates">.
        /// </summary>
        /// <param name="coordinates">Array of two values</param>
        /// <returns></returns>
        public CellState GetCellStatus(int[] coordinates)
        {
            CheckCoordinatesCount(coordinates);
            return GetCellStatus(coordinates[0], coordinates[1]);
        }

        /// <summary>
        /// Checks if coordinates are targeting board
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <exception cref="IndexOutOfBoardException"></exception>
        private static void CheckRange(int x, int y)
        {
            if (x < 0 || x >= XSize || y < 0 || y >= YSize)
            {
                throw new IndexOutOfBoardException($"Indexing cell out of grid. X: {x}, Y: {y}");
            }
        }

        /// <summary>
        /// Checks if array contains two values
        /// </summary>
        /// <param name="coordinates"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidArgumentException"></exception>
        private static void CheckCoordinatesCount(int[] coordinates)
        {
            if (coordinates == null) throw new ArgumentNullException(nameof(coordinates));
            if (coordinates.Length != 2)
                throw new InvalidArgumentException("coordinates");
        }

        /// <summary>
        /// Checks if anyone has three marks in row.
        /// </summary>
        /// <returns>Returns None when the game is stil going, null when it's draw and PlayerOne/PlayerTwo when one of players wins</returns>
        public CellState? CheckWinner()
        {
            // Check rows
            if (_boardState.Any(row => row.All(cell => cell == CellState.PlayerOne)))
                return CellState.PlayerOne;

            if (_boardState.Any(row => row.All(cell => cell == CellState.PlayerTwo)))
                return CellState.PlayerTwo;

            // Check columns
            for (var index = 0; index < XSize; index++)
            {
                if (_boardState[0][index] == _boardState[1][index] && _boardState[1][index] == _boardState[2][index] && _boardState[0][index] != CellState.None)
                    return _boardState[0][index];
            }

            //Check diagonals
            if (_boardState[0][0] == _boardState[1][1] && _boardState[2][2] == _boardState[1][1] && _boardState[1][1] != CellState.None)
                return _boardState[1][1];

            if (_boardState[0][2] == _boardState[1][1] && _boardState[2][0] == _boardState[1][1])
                return _boardState[1][1];

            // Draw
            if (_boardState.All(row => row.All(cell => cell != CellState.None)))
                return null;

            // No winner yet
            return CellState.None;
        }
    }
}